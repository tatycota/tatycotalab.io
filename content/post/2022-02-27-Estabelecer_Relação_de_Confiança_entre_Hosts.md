---
title: Estabelecer relação de confiança entre Hosts 
date: 2022-02-28
---


1- Se ainda não existir, crie a chave rsa na origem da conexão

```
 ssh-keygen -t rsa -f ~/.ssh/id_rsa
```

2- Enviar para o servidor destino do ssh, que estabeleceremos a confiança:

```
 cat ~/.ssh/id_rsa.pub | ssh usuário@192.168.0.5 'cat - >> ~/.ssh/authorized_keys'
```

3- Acrescentar o host destino de conexão no /etc/hosts


