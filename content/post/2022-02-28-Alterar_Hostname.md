---
title: Alterar Hostname
date: 2022-02-28
---

Coloque o nome desejado no arquivo hosts  /etc/hosts. 

Coloque o nome desejado no arquivo hostname /etc/hostname. ...

Ultimo passo e mais importante, coloque o novo nome diretamente na memória. echo "NOME" > /proc/sys/kernel/hostname. 

