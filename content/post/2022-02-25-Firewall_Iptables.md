---
tetle: Firewall Iptables 
date: 2022-02-25
---


Comando para verificar as regras de firewall ativas:

-n  numeric output of addresses and ports
-v  verbose
-L  List

```
iptables -nvL
```


